TARGET := ONScripter-Vita

SFO_TITLE_NAME  := ONScripter
SFO_TITLE_ID    := ONSVITA01
SFO_APP_VER     := 01.00

APP_NAME         := ONScripter
APP_VER          := 1.0
APP_DIR_NAME     := ONScripter
CORE_SOFTWARE    := ONScripter

BETA_BUILD   := 0
BETA_VER     := 0
DEBUG        := 1

BUILD_DATE   := $(shell date +"%Y.%m.%d")

APP_NAME_STR := $(APP_NAME)
APP_VER_STR  := $(APP_VER)
VPK_VER_STR  := $(APP_VER)

ifeq ($(BETA_BUILD), 1)
	APP_VER_STR := $(APP_VER_STR)\ beta$(BETA_VER)
	VPK_VER_STR := $(VPK_VER_STR)_beta$(BETA_VER)
endif

MAKEFILE_PATH      := $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_DIR       := $(dir $(MAKEFILE_PATH))
ROOT_DIR           := $(patsubst %/,%, $(MAKEFILE_DIR))

DEPS_DIR           := $(ROOT_DIR)/deps
DEPS_INCLUDE_DIR   := $(DEPS_DIR)/include
DEPS_LIB_DIR       := $(DEPS_DIR)/lib

VITA_DIR           := $(ROOT_DIR)/vita
BUILD_DIR          := $(ROOT_DIR)/build

VITA_SRC_DIR       := $(VITA_DIR)
VITA_PKG_DIR       := $(ROOT_DIR)/pkg
VITA_BUILD_DIR     := $(BUILD_DIR)/vita

EBOOT_BUILD_PATH   := $(VITA_PKG_DIR)/eboot.bin
SFO_BUILD_PATH     := $(VITA_PKG_DIR)/sce_sys/param.sfo
VPK_BUILD_NAME     := $(TARGET)_v$(VPK_VER_STR).vpk
VPK_BUILD_PATH     := $(ROOT_DIR)/$(VPK_BUILD_NAME)

CORE_NAME          := onscripter
CORE_DIR           := $(ROOT_DIR)/onscripter
CORE_MAKEFILE_DIR  := $(CORE_DIR)
CORE_MAKEFILE_NAME := Makefile.vita
CORE_BUILD_DIR     := $(BUILD_DIR)/onscripter
CORE_TARGET_PATH   := $(VITA_PKG_DIR)/$(CORE_NAME).self

CORE_ARGS := \
	CORE_BUILD_DIR="$(CORE_BUILD_DIR)" \
	CORE_TARGET_PATH="$(CORE_TARGET_PATH)"

INCLUDES := \
	-I$(VITA_DIR) \
	-I$(DEPS_INCLUDE_DIR)

LINKS := \
	-L$(DEPS_LIB_DIR)

VITA_SRC_C := \
	about.c \
	browser.c \
	file.c \
	graphics.c \
	init.c \
	main.c \
	game.c \
	sbrk.c \
	strnatcmp.c \
	ui.c \
	utils.c

VITA_OBJS := $(VITA_SRC_C:.c=.o)
OBJS      := $(addprefix $(VITA_BUILD_DIR)/, $(VITA_OBJS))

SCE_LIBC_SIZE := 4194304

DEFINES := \
	-DAPP_NAME_STR=\"$(APP_NAME_STR)\" \
	-DAPP_VER_STR=\"$(APP_VER_STR)\" \
	-DAPP_DIR_NAME=\"$(APP_DIR_NAME)\" \
	-DBUILD_DATE=\"$(BUILD_DATE)\" \
	-DCORE_SOFTWARE=\"$(CORE_SOFTWARE)\" \
	-DSCE_LIBC_SIZE=$(SCE_LIBC_SIZE)

ifeq ($(DEBUG), 1)
	DEFINES += -DDEBUG
endif

LIBS := \
	-lvita2d \
	-lfreetype \
	-lpng \
	-ljpeg \
	-lz \
	-lm \
	-lc \
	-lScePgf_stub \
	-lSceDisplay_stub \
	-lSceGxm_stub \
	-lSceCtrl_stub \
	-lScePower_stub \
	-lSceRtc_stub \
	-lSceAppUtil_stub \
	-lSceAppMgr_stub \
	-lSceShellSvc_stub \
	-lSceCommonDialog_stub \
	-lSceSysmodule_stub

PREFIX  := arm-vita-eabi
CC      := $(PREFIX)-gcc
CXX     := $(PREFIX)-g++
AS      := $(PREFIX)-as
AR      := $(PREFIX)-ar
OBJCOPY := $(PREFIX)-objcopy
STRIP   := $(PREFIX)-strip
NM      := $(PREFIX)-nm
LD      := $(CXX)

WARNINGS := \
	-Wall \
	-Wno-unused-variable \
	-Wno-unused-but-set-variable \
	-Wno-format-truncation

FLAGS    := $(WARNINGS) $(INCLUDES) $(DEFINES)
CFLAGS   := -O3 $(FLAGS)
CXXFLAGS := $(CFLAGS) -fno-rtti -fno-exceptions
LDFLAGS  := -Wl,-q $(LINKS)

all: create_dir build-core build-vita

create_dir:
	@mkdir -p $(VITA_BUILD_DIR)

build-core:
	cd $(CORE_MAKEFILE_DIR) && make -f $(CORE_MAKEFILE_NAME) $(CORE_ARGS)

build-vita: $(EBOOT_BUILD_PATH) $(SFO_BUILD_PATH)
	@rm -rf $(VPK_BUILD_PATH)
	cd $(VITA_PKG_DIR) && zip -r $(VPK_BUILD_PATH) ./*

$(SFO_BUILD_PATH):
	@mkdir -p $(dir $@)
	vita-mksfoex -s APP_VER="$(SFO_APP_VER)" \
		-s TITLE_ID="$(SFO_TITLE_ID)" "$(SFO_TITLE_NAME)" \
		-d ATTRIBUTE2=12 $@

$(EBOOT_BUILD_PATH): $(VITA_BUILD_DIR)/$(TARGET).velf
	vita-make-fself -c -a 0x2800000000000001 $< $@

$(VITA_BUILD_DIR)/$(TARGET).velf: $(VITA_BUILD_DIR)/$(TARGET).elf
	$(STRIP) -g $<
	vita-elf-create $< $@

$(VITA_BUILD_DIR)/$(TARGET).elf: $(OBJS)
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

$(VITA_BUILD_DIR)/%.o: $(VITA_SRC_DIR)/%.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@

$(VITA_BUILD_DIR)/%.o: $(VITA_SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) $< -o $@

clean-core:
	cd $(CORE_MAKEFILE_DIR) && make -f $(CORE_MAKEFILE_NAME) clean $(CORE_ARGS)

clean-vita:
	rm -rf $(VITA_BUILD_DIR) $(VPK_BUILD_PATH) $(EBOOT_BUILD_PATH) $(SFO_BUILD_PATH)

clean: clean-vita

clean-all: clean-core clean-vita
	rm -rf $(BUILD_DIR)
