/* -*- C++ -*-
 * 
 *  Encoding.h - Character encoding handler
 *
 *  Copyright (c) 2019-2020 Ogapee. All rights reserved.
 *
 *  ogapee@aqua.dti2.ne.jp
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __ENCODING_H__
#define __ENCODING_H__

#ifdef CHARSET_GBK

#define IS_TWO_BYTE(x) \
        ( ((unsigned char)(x) >= (unsigned char)0x81) && ((unsigned char)(x) <= (unsigned char)0xfe) )

#define IS_ROTATION_REQUIRED(x) \
    ( ( !IS_TWO_BYTE(*(x))) || \
        ( *(x) == (unsigned char)0xa1 && *((x)+1) >= 0xb4 && *((x)+1) <= 0xbf ) || \
        ( *(x) == (unsigned char)0xa3 && *((x)+1) >= 0xfc && *((x)+1) <= 0xfe ) || \
        ( *(x) == (unsigned char)0xa1 && *((x)+1) == (unsigned char)0xab ) || \
        ( *(x) == (unsigned char)0xa1 && *((x)+1) == (unsigned char)0xce ) || \
        ( *(x) == (unsigned char)0xa1 && *((x)+1) == (unsigned char)0xad ) || \
        ( *(x) == (unsigned char)0xa1 && *((x)+1) == (unsigned char)0xc2 ) || \
        ( *(x) == (unsigned char)0xa3 && *((x)+1) == (unsigned char)0xdf ) || \
        ( *(x) == (unsigned char)0xa3 && *((x)+1) == (unsigned char)0xa8 ) || \
        ( *(x) == (unsigned char)0xa8 && *((x)+1) == (unsigned char)0x44 ) || \
        ( *(x) == (unsigned char)0xa8 && *((x)+1) == (unsigned char)0x45 ) || \
        ( *(x) == (unsigned char)0xa9 && *((x)+1) == (unsigned char)0x5c ) || \
        ( *(x) == (unsigned char)0xa9 && *((x)+1) == (unsigned char)0x60 ) )

#define IS_TRANSLATION_REQUIRED(x) \
    ( ( *(x) == (unsigned char)0xa1 && *((x)+1) == (unsigned char)0xa2 )   || \
        ( *(x) == (unsigned char)0xa1 && *((x)+1) == (unsigned char)0xa3 ) || \
        ( *(x) == (unsigned char)0xa3 && *((x)+1) == (unsigned char)0xac ) || \
        ( *(x) == (unsigned char)0xa3 && *((x)+1) == (unsigned char)0xae ) )

extern void initGBK2UTF16();
extern unsigned short convGBK2UTF16( unsigned short in );
extern unsigned short convUTF162GBK( unsigned short in );

#else /*CHARSET_SJIS*/

#define IS_TWO_BYTE(x) \
        ( ((x) & 0xe0) == 0xe0 || ((x) & 0xe0) == 0x80 )

#define IS_ROTATION_REQUIRED(x)	\
    (!IS_TWO_BYTE(*(x)) ||                                              \
     (*(x) == (char)0x81 && *((x)+1) == (char)0x50) ||                  \
     (*(x) == (char)0x81 && *((x)+1) == (char)0x51) ||                  \
     (*(x) == (char)0x81 && *((x)+1) >= 0x5b && *((x)+1) <= 0x5d) ||    \
     (*(x) == (char)0x81 && *((x)+1) >= 0x60 && *((x)+1) <= 0x64) ||    \
     (*(x) == (char)0x81 && *((x)+1) >= 0x69 && *((x)+1) <= 0x7a) ||    \
     (*(x) == (char)0x81 && *((x)+1) == (char)0x80) )

#define IS_TRANSLATION_REQUIRED(x)	\
        ( *(x) == (char)0x81 && *((x)+1) >= 0x41 && *((x)+1) <= 0x44 )

extern void initSJIS2UTF16();
extern unsigned short convSJIS2UTF16( unsigned short in );
extern unsigned short convUTF162SJIS( unsigned short in );

#endif

extern int convUTF16ToUTF8( unsigned char dst[4], unsigned short src );
extern unsigned short convUTF8ToUTF16( const char **src );

class Encoding
{
public:
    enum {
#ifdef CHARSET_GBK
        CODE_CP936 = 0,
#else /*CHARSET_SJIS*/
        CODE_CP932 = 0,
#endif
        CODE_UTF8 = 1
    };
    
    Encoding();
    ~Encoding();

    void setEncoding(int code);
    int getEncoding(){ return code; };
    
    char getTextMarker();

    int getBytes(unsigned char ch, int code = -1);
    int getNum(const unsigned char *buf);
    
    unsigned short getUTF16(const char *text, int code = -1);

private:
    int code;
};

#endif // __ENCODING_H__ 

