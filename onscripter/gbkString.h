#ifndef __GBK_STRING_H__
#define __GBK_STRING_H__

#define DEFAULT_SAVE_MENU_NAME "保存"
#define DEFAULT_LOAD_MENU_NAME "读取"
#define DEFAULT_SAVE_ITEM_NAME "进度"

#define MESSAGE_SAVE_EXIST     "%s%s　%s月%s日%s时%s分"
#define MESSAGE_SAVE_EMPTY     "%s%s　－－－－－－－－－－－－"
#define MESSAGE_SAVE_CONFIRM   "%s保存到%s。确认？"
#define MESSAGE_LOAD_CONFIRM   "%s从%s读取。确认？"
#define MESSAGE_RESET_CONFIRM  "重置游戏。确认？"
#define MESSAGE_END_CONFIRM    "结束游戏。确认？"
#define MESSAGE_YES            "是"
#define MESSAGE_NO             "否"
#define MESSAGE_OK             "确定"
#define MESSAGE_CANCEL         "取消"

#define WCHAR_ZERO             "０"
#define WCHAR_SPACES           "　"
#define WCHAR_MINUS            "－"
#define WCHAR_NUMS             "０１２３４５６７８９"
#define WCHAR_LEFT_BRACKETS    "【"
#define WCHAR_RIGHT_BRACKETS   "】"

#define DEFAULT_ENV_FONT       "ＭＳ ゴシック"

#endif