#ifndef __SJIS_STRING_H__
#define __SJIS_STRING_H__

#define DEFAULT_SAVE_MENU_NAME "＜セーブ＞"
#define DEFAULT_LOAD_MENU_NAME "＜ロード＞"
#define DEFAULT_SAVE_ITEM_NAME "しおり"

#define MESSAGE_SAVE_EXIST     "%s%s　%s月%s日%s時%s分"
#define MESSAGE_SAVE_EMPTY     "%s%s　−−−−−−−−−−−−"
#define MESSAGE_SAVE_CONFIRM   "%s%sにセーブします。よろしいですか？"
#define MESSAGE_LOAD_CONFIRM   "%s%sをロードします。よろしいですか？"
#define MESSAGE_RESET_CONFIRM  "リセットします。よろしいですか？"
#define MESSAGE_END_CONFIRM    "終了します。よろしいですか？"
#define MESSAGE_YES            "はい"
#define MESSAGE_NO             "いいえ"
#define MESSAGE_OK             "ＯＫ"
#define MESSAGE_CANCEL         "キャンセル"

#define WCHAR_ZERO             "０"
#define WCHAR_SPACES           "　"
#define WCHAR_MINUS            "−"
#define WCHAR_NUMS             "０１２３４５６７８９"
#define WCHAR_LEFT_BRACKETS    "【"
#define WCHAR_RIGHT_BRACKETS   "】"

#define DEFAULT_ENV_FONT       "ＭＳ ゴシック"

#endif