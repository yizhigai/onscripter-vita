#ifndef __M_UTILS_H__
#define __M_UTILS_H__

#include <stdint.h>

#include <psp2/rtc.h>
#include <psp2/ctrl.h>

#define ANALOG_CENTER 128
#define ANALOG_THRESHOLD 64
#define ANALOG_SENSITIVITY 16

#define SKIP_PRESSED_PSBUTTON_COUNT 30

#define N_PADS 4

enum ControlTypes
{
    LIST_CONTROL_NONE,
    LIST_CONTROL_UP,
    LIST_CONTROL_DOWN,
    LIST_CONTROL_LEFT,
    LIST_CONTROL_RIGHT,
};

enum PadButtons
{
    PAD_LEFT,
    PAD_UP,
    PAD_RIGHT,
    PAD_DOWN,
    PAD_CROSS,
    PAD_CIRCLE,
    PAD_SQUARE,
    PAD_TRIANGLE,
    PAD_L1,
    PAD_R1,
    PAD_L2,
    PAD_R2,
    PAD_L3,
    PAD_R3,
    PAD_SELECT,
    PAD_START,
    PAD_PSBUTTON,
    PAD_ENTER,
    PAD_CANCEL,
    PAD_LEFT_ANALOG_UP,
    PAD_LEFT_ANALOG_DOWN,
    PAD_LEFT_ANALOG_LEFT,
    PAD_LEFT_ANALOG_RIGHT,
    PAD_RIGHT_ANALOG_UP,
    PAD_RIGHT_ANALOG_DOWN,
    PAD_RIGHT_ANALOG_LEFT,
    PAD_RIGHT_ANALOG_RIGHT,
    PAD_N_BUTTONS
};
#define N_LOCAL_BUTTONS PAD_PSBUTTON

typedef struct
{
    SceCtrlData pad;
    int error;
} CtrlDataEntry;

typedef uint8_t Pad[PAD_N_BUTTONS];

extern uint32_t vita_buttons[];

extern CtrlDataEntry ctrl_data_entries[N_PADS];
extern Pad old_pads[N_PADS], current_pads[N_PADS];
extern Pad hold_counts[N_PADS];

extern Pad old_pad, current_pad, pressed_pad, released_pad, hold_pad, hold2_pad;
extern Pad hold_count, hold2_count;

int APP_LOG(char *text, ...);

void readPad();
void resetPad();

int hasEndSlash(const char *path);
int removeEndSlash(char *path);
int addEndSlash(char *path);

void convertUtcToLocalTime(SceDateTime *time_local, SceDateTime *time_utc);
void convertLocalTimeToUtc(SceDateTime *time_utc, SceDateTime *time_local);

void getSizeString(char string[16], uint64_t size);
void getDateString(char string[24], int date_format, SceDateTime *time);
void getTimeString(char string[16], int time_format, SceDateTime *time);

void refreshListPos(int *top_pos, int *focus_pos, int length, int lines);
void controlRefreshListPos(int type, int *top_pos, int *focus_pos, int length, int lines);

void lockHome();
void unlockHome();
void lockUsbConnection();
void unlockUsbConnection();

#endif
