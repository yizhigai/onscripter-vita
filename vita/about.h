#ifndef __M_ABOUT_H__
#define __M_ABOUT_H__

void initAboutDrawInfo();
int enterAbout();
int exitAbout();
int drawAbout();
int ctrlAbout();

#endif
