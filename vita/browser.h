#ifndef __M_BROWSER_H__
#define __M_BROWSER_H__

#define MAX_DIR_LEVELS 128

#define HOME_PATH "home"

void initBrowserDrawInfo();
int initBrowser();
int drawBrowser();
int ctrlBrowser();

int changeToDirectoryFromPath(const char *path);
int changeToDirectoryFromFile(const char *path);

int makePreviewPath(char *path);
int loadPreview();
int unloadPreview();

int currentPathIsFile();
int makeCurFileName(char *name);
int makeCurFilePath(char *path);

#endif
