#ifndef __M_ABOUT_TEXT_H__
#define __M_ABOUT_TEXT_H__

#include "config.h"

char *about_text[] = {
    APP_NAME_STR,
    " * 作者: 一直改 (百度psvita破解吧)",
    " * 版本: " APP_VER_STR "    日期: " BUILD_DATE,
    "",
    "平台:",
    " * " CORE_SOFTWARE,
    "",
    "感谢:",
    " * Unkown (ONScripter)",
    " * Unkown (ONScripter-GBK)",
    " * Team molecule (HENkaku)",
    " * xerpi (vita2dlib)",
    " * TheFlow (vitaShell)",
    " * frangarcj (vita shader)",
    " * ......",
};

#define N_ABOUT_TEXT (sizeof(about_text) / sizeof(char **))

#endif