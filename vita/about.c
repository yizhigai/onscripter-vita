#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "about.h"
#include "about_text.h"
#include "ui.h"
#include "utils.h"
#include "config.h"
#include "texts.h"

#define ABOUT_PADDING 10.0f

#define ABOUT_LINE_SPACE 4.0f
#define ABOUT_TEXT_COLOR ORANGE

InstructionsEntry about_instructions_entries[] = {
    {{STR_BUTTON_CANCEL, NULL}, STR_BACK_TO_BROWSER},
    {{0}, NULL},
};

int about_open = 0;
static int top_pos = 0;

static int text_lines;

static float view_sx, view_sy, view_dx, view_dy, text_sx, text_sy;
static float view_width, view_height, text_width, text_y_space;

void initAboutDrawInfo()
{
    float line_height = UiGetLineHeight();

    view_width = MAIN_FREE_DRAW_WIDTH;
    view_height = MAIN_FREE_DRAW_HEIGHT;

    view_sx = MAIN_FREE_DRAW_SX;
    view_sy = MAIN_FREE_DRAW_SY;
    view_dx = view_sx + view_width;
    view_dy = view_sy + view_height;

    float text_full_height = view_height - ABOUT_PADDING * 2;
    text_y_space = line_height + ABOUT_LINE_SPACE;
    text_lines = (text_full_height + ABOUT_LINE_SPACE) / text_y_space;

    text_sx = view_sx + ABOUT_PADDING;
    text_sy = view_sy + ABOUT_PADDING;
}

int enterAbout()
{
    about_open = 1;

    return 0;
}

int exitAbout()
{
    about_open = 0;

    return 0;
}

int drawAbout()
{
    vita2d_draw_rectangle(view_sx, view_sy, view_width, view_height, DEFALUT_BG_COLOR);

    float sx = text_sx;
    float sy = text_sy;
    int i;
    for (i = top_pos; i < N_ABOUT_TEXT && i < (top_pos + text_lines); i++)
    {
        UiDrawText(sx, sy, ABOUT_TEXT_COLOR, about_text[i]);
        sy += text_y_space;
    }

    drawScrollBar(view_dx - SCROLL_BAR_WIDTH, view_sy, view_height, text_lines, N_ABOUT_TEXT, top_pos);

    return 0;
}

int ctrlAbout()
{
    if (hold_pad[PAD_UP] || hold2_pad[PAD_LEFT_ANALOG_UP])
    {
        if (top_pos > 0)
        {
            top_pos--;
        }
    }
    else if (hold_pad[PAD_DOWN] || hold2_pad[PAD_LEFT_ANALOG_DOWN])
    {
        if (top_pos < (int)(N_ABOUT_TEXT - text_lines))
        {
            top_pos++;
        }
    }

    if (pressed_pad[PAD_CANCEL])
    {
        exitAbout();
    }

    return 0;
}
