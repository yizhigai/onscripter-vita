#ifndef __M_STRING_H__
#define __M_STRING_H__

#define STR_BUTTON_LEFT                "←"
#define STR_BUTTON_UP                  "↑"
#define STR_BUTTON_RIGHT               "→"
#define STR_BUTTON_DOWN                "↓"
#define STR_BUTTON_CROSS               "X" //"×"
#define STR_BUTTON_CIRCLE              "○"
#define STR_BUTTON_SQUARE              "□"
#define STR_BUTTON_TRIANGLE            "△"
#define STR_BUTTON_L                   "L"
#define STR_BUTTON_R                   "R"
#define STR_BUTTON_L2                  "L2"
#define STR_BUTTON_R2                  "R2"
#define STR_BUTTON_L3                  "L3"
#define STR_BUTTON_R3                  "R3"
#define STR_BUTTON_SELECT              "Select"
#define STR_BUTTON_START               "Start"
#define STR_BUTTON_PSBUTTON            "Home"
#define STR_BUTTON_LEFT_ANALOG         "左摇杆"
#define STR_BUTTON_LEFT_ANALOG_LEFT    "左摇杆←"
#define STR_BUTTON_LEFT_ANALOG_UP      "左摇杆↑"
#define STR_BUTTON_LEFT_ANALOG_RIGHT   "左摇杆→"
#define STR_BUTTON_LEFT_ANALOG_DOWN    "左摇杆↓"
#define STR_BUTTON_RIGHT_ANALOG        "右摇杆"
#define STR_BUTTON_RIGHT_ANALOG_LEFT   "右摇杆←"
#define STR_BUTTON_RIGHT_ANALOG_UP     "右摇杆↑"
#define STR_BUTTON_RIGHT_ANALOG_RIGHT  "右摇杆→"
#define STR_BUTTON_RIGHT_ANALOG_DOWN   "右摇杆↓"
#define STR_BUTTON_AND                 "+"
#define STR_BUTTON_OR                  "/"

#define STR_SAFE_MODE_PRINT_0    "当前处于安全模式，请先在HENkaku设置里开启启用不安全自制软件，"
#define STR_SAFE_MODE_PRINT_1    "然后再回来操作。"
#define STR_SAFE_MODE_PRINT_2    "按任意键退出！"

#define STR_BATTERY   "电量"
#define STR_ABOUT     "关于"

#define STR_PARENT_DIRECTORY  "上层目录"
#define STR_OPEN              "打开"
#define STR_CHANGE_DIRECTORY  "跳转目录"

#define STR_BACK_TO_BROWSER   "返回浏览器"

#endif