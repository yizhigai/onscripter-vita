#ifndef __M_CONFIG_H__
#define __M_CONFIG_H__

#define FONT_PGF_NAME        "font.pgf"
#define WALLPAPER_PNG_NAME   "bg.png"
#define SPLASH_PNG_NAME      "splash.png"

#define APP_DATA_DIR         "ux0:data/" APP_DIR_NAME
#define APP_ASSETS_DIR       "app0:assets"

#define CORE_CONFIGS_DIR      APP_DATA_DIR "/configs"
#define CORE_SAVEFILES_DIR    APP_DATA_DIR "/savefiles"
#define CORE_SAVESTATES_DIR   APP_DATA_DIR "/savestates"
#define CORE_SCREENSHOTS_DIR  APP_DATA_DIR "/screenshots"
#define CORE_APP_PATH         "app0:onscripter.self"

#define APP_LOG_PATH   APP_DATA_DIR "/log.txt"
#define LASTFILE_PATH  APP_DATA_DIR "/lastfile.txt"

#endif
