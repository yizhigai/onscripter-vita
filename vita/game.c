#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <psp2/appmgr.h>

#include "game.h"
#include "utils.h"
#include "file.h"
#include "config.h"

int sendBootParams(char *app_path, char *argv[])
{
    int ret = sceAppMgrLoadExec(app_path, (char *const *)argv, NULL);

    return ret;
}

int loadGameWithBootParams(char *app_path, char *game_path, char *save_path)
{
    if (!checkFileExist(app_path))
        return -1;

    char *argv[] = {
        "--root",
        game_path,
        "--save-dir",
        save_path,
        "--arch-mode",
        NULL,
    };
    int ret = sendBootParams(app_path, argv);

    return ret;
}

int loadGame(char *path)
{
    APP_LOG("game_path: %s\n", path);
    WriteFile((LASTFILE_PATH), path, strlen(path) + 1);

    char name[MAX_NAME_LENGTH];
    makeFilename(name, path);
    char save_path[MAX_PATH_LENGTH];
    snprintf(save_path, MAX_PATH_LENGTH, "%s/%s", CORE_SAVEFILES_DIR, name);
    createFolder(save_path);
    APP_LOG("save_path: %s\n", save_path);

    int ret = loadGameWithBootParams(CORE_APP_PATH, path, save_path);

    return ret;
}
