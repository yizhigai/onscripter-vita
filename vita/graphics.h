#ifndef __M_GRAPHICS_H__
#define __M_GRAPHICS_H__

#include "vita2d.h"

int initFonts();
void finishFonts();

void UiStartDrawing();
void UiEndDrawing();

int UiDrawText(int x, int y, unsigned int color, const char *text);
int UiDrawTextf(int x, int y, unsigned int color, const char *text, ...);

float UiGetFontScale();
void UiSetFontScale(float scale);
float UiGetFontHeight();
float UiGetLineHeight();

int UiGetTextWidth(const char *text);
int UiGetTextHeight(const char *text);

void vita2d_draw_empty_rectangle(float x, float y, float w, float h, float l, unsigned int color);

#endif
